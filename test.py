# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = 'xoxb-691774128534-689163842868-La6FnhuUHfCtgcrOUlieKQE2'
# Basic Information으로 들어가서
# Signing Secret 옆의 Show를 클릭한 다음, 복사하여 문자열로 붙여넣습니다
SLACK_SIGNING_SECRET = '09f103571d728c91c82ba8730297bdef'

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

old_list = []
list_href = []
list_href2 = []
for page_num in range(1, 17, 1):
    url = "http://program.tving.com/olive/todaymenu/23/Content/List?page=" + str(page_num)
    req = urllib.request.Request(url)
    # URL 주소에 있는 HTML 코드를 soup에 저장합니다.
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    # 반복문을 사용해 원하는 정보 range(3,23)까지 find("a")["href"] 를 사용해서
    # href 모두 수집하여 list_href에 저장

    for link in soup.find('div', class_='pocThumbListType01 cboth').find_all('a', class_='thumbnail'):
        if 'href' in link.attrs:  # 내부에 있는 항목들을 리스트로 가져옵니다
            list_href.append('http://program.tving.com/' + link.attrs['href'])

    for old_menu in soup.find("div", class_="contentInnerWrap").find_all("p", class_="title"):
        check = old_menu.get_text()
        if "'" in check:
            new_menu = old_menu.get_text().split("'")
        if '<' in check:
            new_menu = old_menu.get_text().replace('>', '<').split('<')

        old_list.append(new_menu[1])

new_list = []

for num in range(1, len(old_list) + 1, 1):
    temp_list = [num, old_list[num - 1], list_href[num - 1]]
    new_list.append(temp_list)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    # 여기에 함수를 구현해봅시다.

    # URL 데이터를 가져올 사이트 url 입력

    if "menu" in text:
        print_list = []
        for print_num in range(0, 150, 1):
            print_list.append(str(new_list[print_num][0]) + '. ' + new_list[print_num][1])

        return u'\n'.join(print_list)

    # 레시피

    if int(text.split()[1]) > 0:
        y = text.split()
        url = new_list[int(y[1]) - 1][2]
        req = urllib.request.Request(url)
        # URL 주소에 있는 HTML 코드를 soup에 저장합니다.
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        ttp = []

        for ttt in soup.find('div', class_='moduleView').find_all('p', 'textField contCenter'):
            ttp.append(ttt.get_text().replace('  ', '').replace(
                '[오늘 뭐 먹지?] 매주 월/목요일 낮12시 & 밤 8시 O′live TV 방송! ■ 올리브 페이스북: www.facebook.com/olivetv ■ 공식 홈페이지: http://program.lifestyler.co.kr/olive/todaymenu',
                '').replace('<br/>', '\n'))
        for ttt in soup.find('div', class_='moduleView').find_all('p', 'textField contLeft'):
            ttp.append(ttt.get_text().strip().replace('  ', '').replace(
                '[오늘 뭐 먹지?] 매주 월/목요일 낮12시 & 밤 8시 O′live TV 방송! ■ 올리브 페이스북: www.facebook.com/olivetv ■ 공식 홈페이지: http://program.lifestyler.co.kr/olive/todaymenu',
                '').replace('.	', '.\n').replace('     ', '').replace('	   ', ' ').replace('<br/>', '\n').replace(
                '. ', '.\n').replace('    ', ' ').replace(
                '오늘 뭐 먹지? 딜리버리 레시피 무수분 토마토 카레[오늘 뭐 먹지? 딜리버리] 매주 화요일 저녁 8시 20분 O′live TV 방송! ■ 올리브 페이스북: www.facebook.com/olivetv ■ 공식 홈페이지: http://program.lifestyler.co.kr/olive/todaymenu',
                ''))

        return u'\n'.join(ttp)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5432)
